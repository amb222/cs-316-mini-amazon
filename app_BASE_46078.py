from flask import Flask, url_for, redirect, session
from flask_sqlalchemy import SQLAlchemy
from flask import render_template
from flask import request
from datetime import datetime
import sqlite3
from sqlalchemy import create_engine
from sqlalchemy.sql import text
import random
import string
import uuid
#up


app = Flask(__name__)
app.secret_key = 'Duke Blue'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config[
    'SQLALCHEMY_DATABASE_URI'] = 'postgres://vtbashbpxwnalz:968c9bc6e911aec03c2cabd95210b4c9a8238574d11a3aab8eae07ae892245f0@ec2-3-234-109-123.compute-1.amazonaws.com:5432/d6p8bg270dvgfv'

db = SQLAlchemy(app)

engine = create_engine(
    'postgres://vtbashbpxwnalz:968c9bc6e911aec03c2cabd95210b4c9a8238574d11a3aab8eae07ae892245f0@ec2-3-234-109-123.compute-1.amazonaws.com:5432/d6p8bg270dvgfv')
engine = db.engine
conn = engine.connect()

# trial

# Need this to be done after db is initialized above
from models import *


# from models import Buyer
# from models import Seller
# from models import Item

@app.route('/')
def start():
    return render_template('login.html')


@app.route('/home')
# @app.route('/home/<name>')
def home():
    # con = sqlite3.connect('test.db')
    # cur = con.cursor()
    # cur.execute("SELECT DISTINCT category_name FROM ITEM")
    # categories = cur.fetchall()

    sql = text("SELECT DISTINCT category_name FROM ITEM")
    result = db.engine.execute(sql)
    categories = result.fetchall()

    # cur = con.cursor()
    # lists all items as recommendations
    # cur.execute("SELECT item.name, item.image, MIN(listed_by.price), item.category_name FROM item NATURAL JOIN listed_by NATURAL JOIN review GROUP BY item.item_id ORDER BY AVG(rating) desc")
    # cur.execute("WITH temp1 AS(SELECT item.item_id, item.name, item.image, MIN(listed_by.price) as price, item.category_name, AVG(rating) as rating FROM item NATURAL JOIN listed_by NATURAL JOIN review GROUP BY item.item_id ORDER BY AVG(rating) desc) SELECT name, image, price, category_name FROM temp1 WHERE (SELECT COUNT(*) FROM temp1 as t WHERE (t.category_name = temp1.category_name) and ((1.0/t.rating) <= (1.0/temp1.rating))) <= 3")
    # items = cur.fetchall()

    sql2 = text(
        "WITH temp1 AS(SELECT item.item_id, item.name, item.image, MIN(listed_by.price) as price, item.category_name, AVG(rating) as rating FROM item NATURAL JOIN listed_by NATURAL JOIN review GROUP BY item.item_id ORDER BY AVG(rating) desc) SELECT name, image, price, category_name FROM temp1 WHERE (SELECT COUNT(*) FROM temp1 as t WHERE (t.category_name = temp1.category_name) and ((1.0/t.rating) <= (1.0/temp1.rating))) <= 3")
    result2 = db.engine.execute(sql2)
    items = result2.fetchall()

    return render_template('home.html', title='Home Page', categories=categories, items=items)

@app.route('/sellerhome')
def sellerhome():
    
    if session['seller'] == True:
        sql = text('SELECT i.name, i.description, i.category_name, l.price, l.count, l.availability , l.seller_email, i.item_id, i.image FROM Item i, Listed_By l where i.item_id = l.item_id')
        result = db.engine.execute(sql)
        items = result.fetchall()
        final = []
        for item in items:
            if item[6] == session['email']:
            
                final.append(item)

    #print(final)
    return render_template('sellerhome.html', title='Home Page', items=final, seller_email= session['email'])


@app.route('/searching', methods=['POST', 'GET'])
def search():
    if request.method == 'POST':
        # try:
        entry = request.form['entry']
        formatted_entry = '%' + request.form['entry'] + '%'

        with sqlite3.connect('test.db') as con:
            cur = con.cursor()
            # WILL HAVE TO CHANGE HOW I DID VARIABLES FOR SQLLITE -> MYSQL
            cur.execute(
                "SELECT item.item_id, item.name, item.image, item.description, item.category_name, AVG(rating) FROM item NATURAL JOIN review NATURAL JOIN listed_by, seller WHERE ((listed_by.seller_email = seller.email) AND ((item.name LIKE (?)) OR (item.category_name LIKE (?)) OR (seller.first_name LIKE (?)) OR (seller.last_name LIKE (?)) OR (item.description LIKE (?)) OR (listed_by.price = (?)))) GROUP BY item.item_id",
                (formatted_entry, formatted_entry, formatted_entry, formatted_entry, formatted_entry, entry))
            search_results = cur.fetchall()

            cur = con.cursor()
            cur.execute(
                "SELECT listed_by.item_id, seller.first_name, seller.last_name, listed_by.availability, listed_by.count, listed_by.price FROM seller, listed_by WHERE (seller.email = listed_by.seller_email)")
            sellinfo = cur.fetchall()

        return render_template('searchpage.html', search_results=search_results, entry=entry, sellinfo=sellinfo)
        # return render_template('searchpage.html', entry = entry)


@app.route('/logininfo')
def getlogininfo():
    return render_template('getlogininfo.html')


# @app.route('/validateuser', methods=['GET', 'POST'])
# def validateuser():
#     msg = ''
#     if request.method == 'POST':
#         email = request.form['email']
#         password = request.form['password']
#         sql = text("SELECT email, password FROM Buyer UNION SELECT email, password FROM Seller")
#         result = db.engine.execute(sql)
#         rows = result.fetchall()
#         for row in rows:
#             if row[0] == email and row[1] == password:
#                 session['loggedin'] = True
#                 session['email'] = email
#                 return render_template('responsevalidateuser.html', user=email, msg="Valid Email/Password")
#                 # I want this to return @app.route('/home')
#         return render_template('getlogininfo.html', msg="Invalid Email/Password")
#     else:
#         return render_template('getlogininfo.html')

@app.route('/validateuser', methods=['GET', 'POST'])
def validateuser():
    msg = ''
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        sql = text("SELECT email, password FROM Buyer")
        result = db.engine.execute(sql)
        rows = result.fetchall()
        for row in rows:
            if row[0] == email and row[1] == password:
                session['loggedin'] = True
                session['email'] = email
                return render_template('responsevalidateuser.html', user=email, msg="Valid Email/Password")
                # I want this to return @app.route('/home')
        sql = text("SELECT email, password FROM Seller")
        result = db.engine.execute(sql)
        rows = result.fetchall()
        for row in rows:
            if row[0] == email and row[1] == password:
                session['loggedin'] = True
                session['seller'] = True
                session['email'] = email
                return render_template('responsevalidateuser.html', user=email, msg="Valid Email/Password")
        return render_template('getlogininfo.html', msg="Invalid Email/Password")
    else:
        return render_template('getlogininfo.html')


@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)


@app.route('/login')
def login(name=None):
    return render_template('login.html', name=name)


@app.route('/enternewbuyer')
def new_buyer():
    return render_template('create-account.html')


@app.route('/enterseller')
def new_seller():
    return render_template('create-account1.html')


@app.route('/addrec', methods=['POST', 'GET'])
def addrec():
    msg = ''
    if request.method == 'POST':
        # try:
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        address = request.form['address']
        email = request.form['email']
        password = request.form['password']
        current_balance = request.form['current_balance']
        secret_question = request.form['secret_question']
        secret_answer = request.form['secret_answer']
        payment_info = request.form['payment_info']

        new_user = Buyer(first_name=first_name, last_name=last_name, address=address, email=email, password=password,
                         current_balance=current_balance, secret_question=secret_question, secret_answer=secret_answer,
                         payment_info=payment_info)
        db.session.add(new_user)
        db.session.commit()
        return render_template('result.html', msg="Account created successfully!")


@app.route('/addrec1', methods=['POST', 'GET'])
def addrec1():
    msg = ''
    if request.method == 'POST':
        # try:
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        address = request.form['address']
        email = request.form['email']
        password = request.form['password']
        current_balance = request.form['current_balance']
        secret_question = request.form['secret_question']
        secret_answer = request.form['secret_answer']
        payment_info = request.form['payment_info']

        new_user = Seller(first_name=first_name, last_name=last_name, address=address, email=email, password=password,
                          current_balance=current_balance, secret_question=secret_question, secret_answer=secret_answer,
                          payment_info=payment_info)
        db.session.add(new_user)
        db.session.commit()
        return render_template('result.html', msg="Account created successfully!")


#        with sqlite3.connect('test.db') as con:
#            cur = con.cursor()
#            cur.execute(
#                'INSERT INTO Buyer(first_name, last_name, address, email, password, current_balance, secret_question, secret_answer, payment_info) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
#                (first_name, last_name, address, email, password, current_balance, secret_question, secret_answer,
#                 payment_info))
#            con.commit()
#            msg = "Account successfully created!"


# except:
# con.rollback()
# msg = 'Error in insert operation'
# finally:
# return render_template('result.html', msg=msg)
# con.close()


@app.route('/getbuyeremail')
def getbuyeremail():
    return render_template('getbuyeremail.html', title='Get Buyer Email')


# @app.route('/buyer')
# def buyers():
#     if session['loggedin'] == True:
#         con = sqlite3.connect('test.db')
#         cur=con.cursor()
#         cur.execute("SELECT * FROM Buyer")
#         rows=cur.fetchall()
#         return render_template('buyers.html',title='Buyers list', rows=rows)

@app.route('/buyer')
def buyers():
    sql = text('select * from buyer')

    result = db.engine.execute(sql)

    rows = result.fetchall()

    return render_template('buyers.html', title='Buyers list', rows=rows)


# @app.route('/show_buyer2/<buyeremail>')
# def show_buyer(buyeremail):
#     if session['loggedin'] == True:
#         # cur = con.cursor()
#         email1 = session['email']
#
#         sql = text("SELECT email, first_name, last_name, addresss,current_balance, payment_info  FROM Buyer")
#         result = db.engine.execute(sql)
#         rows = result.fetchall()
#
#         for row in rows:
#             if row[0] == email1:
#                 email1 = row[0]
#                 firstname = row[1]
#                 lastname = row[2]
#                 address = row[3]
#                 c_balance = row[4]
#                 p_info = row[5]
#                 return render_template("show_buyer.html", buyer=email1, firstname=firstname, lastname=lastname,address=address,c_balance=c_balance,p_info=p_info)
#             #buyer = Buyer.query.filter_by(email=email).first()
#             # buyer = Buyer.query.filter_by(email=buyeremail).first()
#             # e1 = Buyer.query.filter_by(email='email1').first()
#
#             # cur.execute("SELECT * FROM Buyer where email= :buyeremail")
#             # rows = cur.fetchall()


@app.route('/show_buyer')
def show_buyer():
    if session['loggedin'] == True:
        # cur = con.cursor()
        email = request.args.get('buyeremail')
        print(email)
        buyer = Buyer.query.filter_by(email=email).first()
        # buyer = Buyer.query.filter_by(email=buyeremail).first()
        # e1 = Buyer.query.filter_by(email='email1').first()

        # cur.execute("SELECT * FROM Buyer where email= :buyeremail")
        # rows = cur.fetchall()
        return render_template("show_buyer.html", buyer=buyer)

        sql = text("SELECT email, first_name, last_name, address,current_balance, payment_info,secret_question, secret_answer  FROM Buyer")
        result = db.engine.execute(sql)
        rows = result.fetchall()
        for row in rows:
            if row[0] == email1:
                email1 = row[0]
                firstname = row[1]
                lastname = row[2]
                address = row[3]
                c_balance = row[4]
                p_info = row[5]
                #sq=row[6]
                #sa=row[7]
                return render_template("show_buyer.html", buyer=email1, firstname=firstname, lastname=lastname,address=address,c_balance=c_balance,p_info=p_info)
    else:
        return redirect(url_for('login'))


@app.route('/forgotpassword')
def forgotpassword():
    return render_template('getsqanswer.html')


@app.route('/checkans', methods=['GET', 'POST'])
def checkans():
    msg = ''
    if request.method == 'POST':
        email = request.form['email']
        secret_question = request.form['secret_question']
        secret_answer = request.form['secret_answer']

        # con = sqlite3.connect('test.db')
        # cur = con.cursor()
        # cur.execute("SELECT email, secret_question, secret_answer, password  FROM Buyer")
        # rows = cur.fetchall()
        sql = text("SELECT email, secret_question, secret_answer, password  FROM Buyer")
        result = db.engine.execute(sql)
        rows = result.fetchall()

        for row in rows:
            if row[0] == email:
                if row[1] == secret_question and row[2] == secret_answer:
                    passwd = row[3]
                    return render_template('showpassword.html', buyer=email, passwd=passwd, msg="Your Password is !!")
                else:
                    return render_template('getsqanswer.html', buyer=email,
                                           msg="Your Question/Answer did not match. Try Again.")
        return render_template('getsqanswer.html', buyer=email, msg="Your Email did not match. Try Again.")
    else:
        return render_template('getsqanswer.html')


@app.route('/items')
def items():
    con = sqlite3.connect('test.db')
    cur = con.cursor()
    cur.execute(
        "SELECT i.name, i.description, i.category_name, l.price, l.count FROM Item i, Listed_By l where i.item_id = l.item_id")
    rows = cur.fetchall()
    return render_template('items.html', title='Items list', rows=rows)


@app.route('/new_item')
def additem():
      return render_template("new_item.html")


@app.route('/deleteitem/<item_id>', methods = ['GET', 'POST'])
def deleteitem(item_id):
    if request.method == 'POST':
        seller_email = session['email']
        ListedBy.query.filter(ListedBy.seller_email == session['email'], ListedBy.item_id == item_id).delete()
        db.session.commit()
        Item.query.filter(Item.item_id == item_id).delete()
        db.session.commit()

    return render_template('result1.html', msg="Item deleted successfully!")

@app.route('/edititem/<item_id>', methods = ['GET', 'POST'])
def edititem(item_id):
    if request.method == 'POST':
        sql = text('SELECT i.item_id, i.name, i.description, i.category_name, l.price, l.count, l.availability , l.seller_email, i.image FROM Item i, Listed_By l where i.item_id = l.item_id')
        result = db.engine.execute(sql)
        items = result.fetchall()
        final = []
        for item in items:
            if item[7] == session['email'] and item[0] == item_id:
            
                final.append(item)
    return render_template("edititem.html", items = final)

@app.route('/modifieditem/<item_id>', methods=['POST', 'GET'])
def modifieditem(item_id):
    if request.method == 'POST':
        listing = ListedBy.query.filter(ListedBy.seller_email == session['email'], ListedBy.item_id == item_id).first()
        item = Item.query.filter(Item.item_id == item_id).first()
        try:
            val = int(request.form['price'])
        except ValueError:
            try:
                val = float(request.form['price'])
            except ValueError:
                return render_template('result1.html', msg="Invalid price - Please try again!")
        try:
            val = int(request.form['count'])
        except ValueError:
            return render_template('result1.html', msg="Invalid count - Please try again!")
        try:
            val = int(request.form['availability'])
        except ValueError:
            return render_template('result1.html', msg="Invalid availability - Please try again!")
        listing.price = request.form['price']
        listing.count = request.form['count']
        listing.availability = request.form['availability']
        item.name = request.form['itemname']
        item.description = request.form['description']
        item.category_name = request.form['category_name']
        item.image = request.form['image']
        
        db.session.commit()
        
        return render_template('result1.html', msg="Item modified successfully!")

@app.route('/addingitem', methods=['POST', 'GET'])
def addingitem():
    if request.method == 'POST':
        item_id = (uuid.uuid4())
        name = request.form['itemname']
        image = request.form['image']
        description = request.form['description']
        category_name = request.form['category_name']
        seller_email = session['email']
        price = request.form['price']
        count = request.form['count']
        availability = request.form['availability']
        try:
            val = int(price)
        except ValueError:
            try:
                val = float(price)
            except ValueError:
                return render_template('result1.html', msg="Invalid price - Please try again!")
        try:
            val = int(count)
        except ValueError:
            return render_template('result1.html', msg="Invalid count - Please try again!")
        try:
            val = int(availability)
        except ValueError:
            return render_template('result1.html', msg="Invalid availability - Please try again!")

        new_item = Item(item_id = item_id, name=name, image=image, description=description, category_name = category_name)
        new_listing = ListedBy(item_id=item_id, seller_email=seller_email, price=price, count=count,availability=availability)
        db.session.add(new_item)
        db.session.commit()
        db.session.add(new_listing)
        db.session.commit()
        
        return render_template('result1.html', msg="Item created successfully!")

##the next two routes, /cart, i want to make sure these are correct - Aissatu(??)

### AVERY: I commented out this cart app route so that Flask would run

#@app.route('/cart')           
#def cart():
#    con = sqlite3.connect('test.db')
#    cur = con.cursor()
#    buyer = 'hannah.rogers@gmail.com'
#    getCartItems = "SELECT i.name, i.image, s.first_name, s.last_name, l.price, c.quantity, i.item_id, s.email FROM Item i, Listed_By l, Seller s, Cart c WHERE c.buyer_email = ? AND c.item_id = l.item_id AND c.item_id = i.item_id AND c.seller_email = s.email AND c.seller_email = l.seller_email"
#    cur.execute(getCartItems, (buyer, ))
#    rows = cur.fetchall()
#    return render_template("new_item.html")

@app.route('/cart')
@app.route('/cart/<errmsg>')          
def cart(errmsg=""):
    if session['loggedin'] == False:
        return redirect(url_for('login'))
    buyer = session['email']
    getCartItems = "SELECT i.name, i.image, s.first_name, s.last_name, l.price, c.quantity, i.item_id, s.email, b.current_balance FROM Item i, Listed_By l, Seller s, Cart c, Buyer b WHERE c.buyer_email = b.email AND c.buyer_email = %s AND c.item_id = l.item_id AND c.item_id = i.item_id AND c.seller_email = s.email AND c.seller_email = l.seller_email"
    results = db.engine.execute(getCartItems, (buyer, ))
    rows = results.fetchall()
    totalPrice = 0
    for row in rows:
        totalPrice += row[4] * row[5]
    return render_template('cart.html', rows=rows, totalPrice = '%.2f' % totalPrice, errmsg = errmsg, email= buyer)

@app.route('/removeFromCart/<itemid>/<selleremail>')
def removeFromCart(itemid,selleremail):
    seller = selleremail.replace("%", "@")
    item = int(itemid)
    buyer = session['email']
    try:
        deleteQuery = "DELETE FROM Cart WHERE buyer_email = %s AND item_id = %s AND seller_email = %s"
        db.engine.execute(deleteQuery, (buyer,item,seller,))
        db.engine.commit()
        print("removed successfully")
    except:
        print("error")
    return redirect(url_for('cart'))


@app.route('/addquantity/<itemid>/<selleremail>')
def addquantity(itemid,selleremail):
    seller = selleremail.replace("%", "@")
    item = int(itemid)
    buyer = session['email']
    try:
        getQuantity = "SELECT Cart.quantity FROM Cart WHERE buyer_email = %s AND item_id = %s AND seller_email = %s"
        results = db.engine.execute(getQuantity, (buyer,item,seller,))
        curQuantity = results.fetchall()[0][0]
        print(curQuantity)
        updQuantity = curQuantity + 1
        updateQuantity = "UPDATE Cart SET quantity = %d WHERE buyer_email = %s AND item_id = %s AND seller_email = %s"
        db.engine.execute(updateQuantity, (updQuantity,buyer,item,seller,))
        db.engine.commit()
        print("add successfully")
    except:
        print("error")
    return redirect(url_for('cart'))


@app.route('/minusquantity/<itemid>/<selleremail>')
def minusquantity(itemid,selleremail):
    seller = selleremail.replace("%", "@")
    item = int(itemid)
    buyer = session['email']
    try:
        getQuantity = "SELECT Cart.quantity FROM Cart WHERE buyer_email = %s AND item_id = %s AND seller_email = %s"
        results = db.engine.execute(getQuantity, (buyer,item,seller,))
        curQuantity = results.fetchall()[0][0]
        print(curQuantity)
        if curQuantity == 1:
            return redirect(url_for('removeFromCart', itemid=itemid, selleremail=selleremail))
        updQuantity = curQuantity - 1
        updateQuantity = "UPDATE Cart SET quantity = %d WHERE buyer_email = %s AND item_id = %s AND seller_email = %s"
        db.engine.execute(updateQuantity, (updQuantity,buyer,item,seller, ))
        db.engine.commit()
        print("updated successfully")
    except:
        print("error")
    return redirect(url_for('cart'))


@app.route('/checkout/<totalPrice>')
def checkout(totalPrice):
    buyer = session['email']
    #CHECK IF USER HAS ENOUGH FUNDS
    getBuyerBal = "SELECT b.current_balance FROM Buyer b WHERE email = %s "
    results = db.engine.execute(getBuyerBal, (buyer, ))
    balance = results.fetchall()[0][0]
    print(balance)
    print(totalPrice)
    if balance < float(totalPrice):
        print("Insufficient Funds")
        return redirect(url_for('cart', errmsg = "Insufficient Funds"))
    else:
        #CHECK IF ALL SELLERS HAVE SUFFICIENT AMOUNT OF ITEMS LEFT
        getSeller = "SELECT l.item_id, l.seller_email, s.first_name, s.last_name, l.count, i.name FROM listed_by l, Cart c, Seller s, Item i WHERE c.buyer_email = %s AND c.item_id = l.item_id AND c.seller_email = l.seller_email AND c.quantity > l.count AND c.seller_email = s.email AND i.item_id=c.item_id"
        results = db.engine.execute(getSeller, (buyer, ))
        rows = results.fetchall()
        if len(rows) > 0:
             print("Seller does not have enough")
             return redirect(url_for('cart', errmsg = "Insufficient number of copies of an item, please buy from other sellers"))
    
    #UPDATING BUYER'S BALANCE
    results = db.engine.execute(getBuyerBal, (buyer, ))
    balance = results.fetchall()[0][0]
    updBalance = balance - float(totalPrice)
    updateBalance = "UPDATE Buyer SET current_balance = %d WHERE email = %s"
    db.engine.execute(updateBalance, (updBalance,buyer, ))
    db.engine.commit()
    print("update buyer's balance successfully")

    getListings = "SELECT c.seller_email, c.item_id, c.quantity, l.count, l.price FROM Cart c, listed_by l WHERE c.seller_email = l.seller_email AND c.item_id = l.item_id AND c.buyer_email = %s"
    results = db.engine.execute(getListings, (buyer, ))
    listings = results.fetchall()
    for listing in listings:
        seller = listing[0]
        item = listing[1]
        # UPDATE LISTEDBY COUNT
        count = listing[3]
        updCount = count - listing[2]
        updateCount = "UPDATE listed_by SET count = %d WHERE seller_email = %s AND item_id = %s"
        db.engine.execute(updateCount, (updCount,seller,item, ))
        db.engine.commit()
        print("update count successfully")
        #UPDTAE SELLER'S BALANCE
        getBalance = "SELECT s.current_balance FROM Seller s WHERE email = %s"
        results = db.engine.execute(getBalance, (seller,))
        balance2 = results.fetchall()[0][0]
        updBalance2 = balance2 + (listing[2] * listing[4])
        updateBalance2 = "UPDATE Seller SET current_balance = %d WHERE email = %s"
        db.engine.execute(updateBalance2, (updBalance2,seller, ))
        db.engine.commit()
        print("update seller's balance successfully")
        #INSERTING ALL PURCHASED ITEM INTO BUYS TABLE
        insertQ = "INSERT INTO Buys(buyer_email, item_id, seller_email, date_bought, quantity) VALUES (%s ,%s, %s, %s, %d)"
        db.engine.execute(insertQ, (buyer,item,seller,datetime.now(),listing[2], ))
        db.engine.commit()
        print("inserted successfully into buys table")
        #DELETING ALL ITEMS BOUGHT FROM CART
        deleteQuery = "DELETE FROM Cart WHERE buyer_email = %s AND item_id = %s AND seller_email = %s"
        db.engine.execute(deleteQuery, (buyer,item,seller,))
        db.engine.commit()
        print("removed successfully")

    return render_template('success.html', email = buyer)


@app.route('/purchased')
def purchased():
    if session['loggedin'] == False:
        return redirect(url_for('login'))
    buyer = session['email']
    getHistory = "SELECT i.image, i.name, s.first_name, s.last_name, l.price, b.quantity, b.date_bought FROM Buys b, Seller s, listed_by l, Item i WHERE b.buyer_email = %s AND s.email = b.seller_email AND b.item_id = i.item_id AND b.item_id = l.item_id AND b.seller_email = l.seller_email"   
    results = db.engine.execute(getHistory, (buyer, ))
    rows = results.fetchall()
    return render_template('purchased.html', rows=rows, email = buyer)


@app.route('/sold')
def sold():
    if session['loggedin'] == False:
        return redirect(url_for('login'))
    seller = session['email']
    getTradeHistory = "SELECT i.image, i.name, u.first_name, u.last_name, l.price, b.quantity, b.date_bought FROM Buys b, Buyer u, listed_by l, Item i WHERE b.seller_email = %s AND u.email = b.buyer_email AND b.item_id = i.item_id AND b.item_id = l.item_id AND b.seller_email = l.seller_email"
    results = db.engine.execute(getTradeHistory, (seller, ))
    rows = results.fetchall()
    return render_template('sold.html', rows=rows, email = seller)

@app.route('/updatebalance')
@app.route('/updatebalance/<msg>')
def updatebalance(msg=""):
    if session['loggedin'] == False:
        return redirect(url_for('login'))
    buyer = session['email']
    getBal = "SELECT b.current_balance, b.payment_info FROM Buyer b WHERE b.email = %s"   
    results = db.engine.execute(getBal, (buyer, ))
    rows = results.fetchall()
    return render_template('updatebalance.html', rows=rows, msg= msg, email = buyer)

@app.route('/addbalance', methods=['POST', 'GET'])
def addbalance():
    if request.method == 'POST':
        add = request.form['add']
        buyer = session['email']
        getBuyerBal = "SELECT b.current_balance FROM Buyer b WHERE email= %s"
        results = db.engine.execute(getBuyerBal, (buyer, ))
        balance = results.fetchall()[0][0]
        try:
            int(add)
        except:
            return redirect(url_for('updatebalance', msg = "Try Again"))
        if (int(add) < 0):
            return redirect(url_for('updatebalance', msg = "Try Again"))
        updBalance = balance + int(add)
        updateBalance = "UPDATE Buyer SET current_balance = %s WHERE email = %s"
        db.engine.execute(updateBalance, (updBalance,buyer, ))
        db.session.commit()
        print("update buyer's balance successfully")
    return redirect(url_for('updatebalance', msg = "Success!"))







