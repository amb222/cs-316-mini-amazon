from sqlalchemy import sql, orm
from datetime import datetime
from app import db

class Buyer(db.Model):
    __tablename__ = 'buyer'
    email = db.Column('email', db.String, primary_key = True)
    password = db.Column('password', db.String(20), nullable=False)
    picture = db.Column('picture', db.String, default = 'default.jpg')
    first_name = db.Column('first_name', db.String(50), nullable = False)
    last_name = db.Column('last_name', db.String(50), nullable=False)
    address = db.Column('address', db.String(100), nullable = False)
    current_balance = db.Column('current_balance', db.Integer, nullable=False)
    secret_question = db.Column('secret_question', db.String(50), nullable = False)
    secret_answer = db.Column('secret_answer', db.String(50), nullable = False)
    payment_info = db.Column('payment_info', db.String(50), nullable=False)
    #posts = db.relationship('Post', backref = 'author', lazy = True)
    # def __repr__(self):
    #     return '<Buyer %r %r %r>' % self.email,self.fist_name,self.last_name

class Seller(db.Model):
    __tablename__ = 'seller'
    email = db.Column('email', db.String, primary_key=True)
    password = db.Column('password', db.String(20), nullable=False)
    picture = db.Column('picture', db.String, default='default.jpg')
    first_name = db.Column('first_name', db.String(50), nullable=False)
    last_name = db.Column('last_name', db.String(50), nullable=False)
    address = db.Column('address', db.String(100), nullable=False)
    current_balance = db.Column('current_balance', db.Integer, nullable=False)
    secret_question = db.Column('secret_question', db.String(50), nullable=False)
    secret_answer = db.Column('secret_answer', db.String(50), nullable=False)
    payment_info = db.Column('payment_info', db.String(50), nullable=False)

class Categories(db.Model):
    __tablename__ = 'categories'
    name = db.Column('name', db.String, primary_key=True)

class Item(db.Model):
    __tablename__ = 'item'
    item_id = db.Column('item_id', db.String, primary_key = True)
    name = db.Column('name', db.String, nullable=False)
    image = db.Column('image', db.String, default = 'default.jpg')
    description = db.Column('description', db.String, nullable = False)
    #NEED TO RELATE TO CATEGORIES
    category_name = db.Column('category_name', db.String, db.ForeignKey(Categories.name), nullable = False)

    def __init__(self, item_id, image, name, description, category_name):
        self.item_id = item_id
        self.name=name
        self.image = image
        self.description = description
        self.category_name = category_name

class Review(db.Model):
    __tablename__ = 'review'
    review_id = db.Column('review_id', db.Integer, primary_key=True, nullable=False)
    #RELATE TO ITEM TABLE
    item_id = db.Column('item_id', db.String, db.ForeignKey(Item.item_id),nullable=False)
    # RELATE TO BUYER TABLE
    buyer_email = db.Column('buyer_email', db.String, db.ForeignKey(Buyer.email), nullable=False)
    rating = db.Column('rating', db.Integer, nullable=False)
    comments = db.Column('comments', db.String)

class ListedBy(db.Model):
    __tablename__ = 'listed_by'
    #RELATE TO ITEM TABLE
    item_id = db.Column('item_id', db.String, db.ForeignKey(Item.item_id), primary_key=True)
    # RELATE TO SELLER TABLE
    seller_email = db.Column('seller_email', db.String, db.ForeignKey(Seller.email), primary_key=True)
    availability = db.Column('availability', db.Integer, nullable=False)
    price = db.Column('price', db.Float, nullable=False)
    count = db.Column('count', db.Integer, nullable=False)
    
    def __init__(self, item_id, seller_email, availability, price, count):
        self.item_id = item_id
        self.seller_email = seller_email
        self.availability = availability
        self.price = price
        self.count = count

#added unique = true to these; this seemed to solve the problem but idk
class Cart(db.Model):
    __tablename__ = 'cart'
    # RELATE TO BUYER TABLE
    buyer_email = db.Column('buyer_email', db.String, db.ForeignKey(Buyer.email), primary_key=True)
    #RELATE TO ITEM TABLE
    item_id = db.Column('item_id', db.String, db.ForeignKey(Item.item_id), primary_key=True)
    # RELATE TO SELLER TABLE
    seller_email = db.Column('seller_email', db.String, db.ForeignKey(Seller.email), primary_key=True)
    quantity = db.Column('quantity', db.Integer, nullable=False)

    
#if they actually purchase it, date_bought will be filled
class Buys(db.Model):
    __tablename__ = 'buys'
    # RELATE TO BUYER TABLE
    buyer_email = db.Column('buyer_email', db.String, db.ForeignKey(Buyer.email), primary_key=True)
    #RELATE TO ITEM TABLE
    item_id = db.Column('item_id', db.String, db.ForeignKey(Item.item_id), primary_key=True)
    # RELATE TO SELLER TABLE
    seller_email = db.Column('seller_email', db.String, db.ForeignKey(Seller.email), primary_key=True)
    quantity = db.Column('quantity', db.Integer, nullable=False)
    date_bought = db.Column('date_bought', db.DateTime, default=datetime.now, primary_key=True)
